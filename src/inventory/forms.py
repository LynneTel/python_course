from django import forms


class ItemPostForm(forms.Form):
    title = forms.CharField(max_length=25)
    description = forms.CharField(widget=forms.Textarea)
    amount = forms.IntegerField()