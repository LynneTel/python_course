from django.shortcuts import render, get_object_or_404, redirect
from django.http import Http404, HttpResponseRedirect
from inventory.models import Item

from inventory.forms import ItemPostForm


def index(request):
    items = Item.objects.exclude(amount=0)
    return render(request, 'inventory/index.html', {
        'items':items,
    })


def item_detail(request, id):
    try:
        item = Item.objects.get(id=id)

    except Item.DoesNotExist:
        raise Http404('This items is not in the table')

    return render(request, 'inventory/item_detail.html', {
        'item':item,
    })


def item_create(request):
    if request.method == "POST":
        form = ItemPostForm(request.POST)
        if form.is_valid():
            cd = form.cleaned_data
            title = cd["title"]
            description = cd["description"]
            amount = cd["amount"]
            Item.objects.create(title=title, description=description, amount=amount)
            return HttpResponseRedirect('/')
    else:
        form = ItemPostForm()
        context = {
            "form":form,
        }
    return render(request, 'inventory/item_create.html', context)


def item_update(request, id):
    item = get_object_or_404(Item, id=id)

    if request.method == "POST":
        form = ItemPostForm(request.POST)
        if form.is_valid():
            cd = form.cleaned_data
            item.title = cd["title"]
            item.description = cd["description"]
            item.amount = cd["amount"]
            item.save()
            return redirect('index')
    else:
        formdata = {'title':item.title, 'description':item.description, 'amount':item.amount}
        form = ItemPostForm(initial=formdata)
        context = {
            "form":form,
        }
    return render(request, 'inventory/item_update.html', context)


def item_delete(request, id):
    item = get_object_or_404(Item, id=id)
    if request.method == "POST":
        print("POST DELETE")
        try:
            item.delete()
            print("item deleted")

        except Item.DoesNotExist:
            raise Http404('This items is not in the table')
            print("item not deleted")

    return redirect('index')
